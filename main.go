package main

import (
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

var (
	uploadUrl  = "/upload/"
	uploadFldr = "." + uploadUrl
	indexHTM   = []byte(`<html><head></head>
	<input type="file" id="filepicker" name="fileList" webkitdirectory multiple>
	<div><ul id="listing"></ul></div>
	<div id="upload"></div>
	<script>
		var xhr = new XMLHttpRequest()
		filepicker.onchange = function(evt){
			upload.innerHTML = ''
			var files = evt.target.files
			for (var i=0; i<files.length; i++) {
				var file = files[i]
				var path = file.webkitRelativePath
				var item = document.createElement('li')
				item.innerHTML = path
				listing.appendChild(item)
				xhr.open('POST', '/upload_folder?path='+path, false)
				xhr.send(file)
			}
			upload.innerHTML = '<a href="` + uploadUrl + `" target="_blank">SHOW UPLOAD</a>'
		}
	</script>
</html>`)
)

func index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	w.Write(indexHTM)
}
func uploadFolder(w http.ResponseWriter, r *http.Request) {
	url := r.URL
	println("pathQuery:", url.RawQuery)
	query := strings.Split(url.RawQuery, "=")
	if len(query) == 2 && query[0] == "path" {
		path := uploadFldr + query[1]
		dirLen := strings.LastIndex(path, "/")
		if dirLen != -1 {
			dir := path[:dirLen]
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				println("0", err.Error())
			} else {
				println("body:", string(body))
				err = os.MkdirAll(dir, 0644)
				if err != nil {
					println("1", err.Error())
				} else {
					err = os.WriteFile(path, body, 0644)
					if err != nil {
						println("2", err.Error())
					}
				}
			}
		}
	}
	w.Write([]byte(""))
}
func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/upload_folder", uploadFolder)
	http.Handle(uploadUrl, http.StripPrefix(uploadUrl, http.FileServer(http.Dir(uploadFldr))))
	http.ListenAndServe(":8080", nil)
}
